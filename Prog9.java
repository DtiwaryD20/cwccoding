
import java.math.BigInteger;
public class Prog9{
	public static BigInteger fact(int n) {
		BigInteger result = BigInteger.ONE;
		for (int i = 2;i<=n;i++) {
			result = result.multiply(BigInteger.valueOf(i));
		}
		return result;
	}
	public static int digisum(BigInteger num) {
	int s=0;
	while(!num.equals(BigInteger.ZERO)) {
		s+=num.mod(BigInteger.TEN).intValue();
		num=num.divide(BigInteger.TEN);
	}
	return s;
	
	}
	public static void main(String []args) {
		
		int sum=digisum(fact(100));
		System.out.println(sum);
		
	}
}