
import java.util.Scanner;
public class prog43
{ public static void  convert(String s)
      {
        int num = Integer.parseInt(s);
           String hexa = Integer.toHexString(num);
           System.out.println("HexaDecimal Value is : " + hexa);
           String octal = Integer.toOctalString(num);
           System.out.println("Octal Value is : " + octal);
           String binary = Integer.toBinaryString(num);
           System.out.println("Binary Value is : " + binary);
       }

    public static void main(String[] args) {
       Scanner sc = new Scanner(System.in);
       System.out.println("Entre a number");
       String s =sc.nextLine();
       convert(s);
    }
}