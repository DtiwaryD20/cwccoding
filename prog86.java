
public class prog86 {
	     
	    public static void main (String[] args)
	    {
			// TODO Auto-generated method stub
	        System.out.println("Kaprekar numbers less than 1000 are:");
	         
	        for (int i=1; i<1000; i++)
	            if (kapr(i))
	                 System.out.print(i + " ");
	        

	}
	    
	    public static boolean kapr(int n)
	    {
	        if (n == 1)
	           return true;
	        int sq = n * n;
	        int count= 0;
	        while (sq!=0)
	        {
	            count++;
	            sq/=10;
	        }
	      
	        sq=n*n;
	        for (int r=1;r<count;r++)
	        {
	             int p= (int) Math.pow(10,r);
	             if (p==n)
	                continue;
	             int sum=sq/p+sq%p;
	             if (sum == n)
	               return true;
	        }
	        return false;
	    }

}
