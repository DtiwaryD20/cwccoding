
import java.util.Scanner;
public class prog32 {

	public static int fact(int k)
	{
		int p=1;
		for(int i=1;i<=k;i++)
		{
			p=p*i;
		}
		return p;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter n and m to calculate binomial formula:");
		int n=sc.nextInt();
		int m=sc.nextInt();
		int bform=fact(n)/(fact(m)*fact(n-m));
		System.out.println("Binomial co-efficient :nCm = "+bform);

	}

}