import java.util.Scanner;
public class prog22 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc=new Scanner(System.in);
		int n1,n2,p=0;
	    System.out.println("Enter 2 numbers to be multiplied:");
	    n1=sc.nextInt();
	    n2=sc.nextInt();
	    while (n2!=0)
	    {
	        if ((n2&1)!=0)
	        {
	          p=p+n1;
	        }
	        n1<<= 1;
	        n2>>= 1;                    
	    }
	    System.out.println("The required product using bitwise operators is: "+p);
	}

}

